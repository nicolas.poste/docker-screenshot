const puppeteer = require('puppeteer');
const yaml = require('js-yaml');
const fs = require('fs');

async function shot(ymlData) {

    const browser = await puppeteer.launch({
        args: [
            '--no-sandbox',
            '--disable-setuid-sandbox',
            '--disable-web-security',
        ],
        ignoreHTTPSErrors: true
    });

    const defaultSize = {
        width: 1280,
        height: 1024
    };

    const page = await browser.newPage();
    let headerSetter = undefined;

    function createRandomFolder() {
        const newFolder = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '/';
        fs.mkdirSync(newFolder, {recursive: true});
        return newFolder;
    }

    const folder = createRandomFolder();

    function setCookies(ymlCookies, cookies) {
        if (cookies.length) {
            page.deleteCookie(...cookies);
        }
        cookies = [];
        if (ymlCookies) {
            for (const cookie in ymlCookies) {
                if (ymlCookies.hasOwnProperty(cookie)) {
                    const cookie1 = ymlCookies[cookie];
                    cookie1.name = cookie;
                    cookies.push(cookie1);
                }
            }
            page.setCookie(...cookies);
        }
        return cookies;
    }

    function headersSetter(siteHeaders) {
        return request => {
            let headers = request.headers();
            for (let [key, value] of Object.entries(siteHeaders)) {
                headers[key] = value;
            }
            request.continue({ headers });
        };
    }

    async function manageHeaders(siteHeaders) {
        if (headerSetter) {
            page.removeListener('request', headerSetter);
            headerSetter = undefined;
        }
        if (siteHeaders) {
            headerSetter = headersSetter(siteHeaders);
            page.on('request', headerSetter);
            await page.setRequestInterception(true)
        } else {
            await page.setRequestInterception(false)
        }
    }

// Get document, or throw exception on error
    try {
        const doc = yaml.safeLoad(ymlData);
        let cookies = [];
        for (const k in doc) {
            if (doc.hasOwnProperty(k)) {
                const site = doc[k];
                console.info('Browsing ' + k + ": " + doc[k].url);
                cookies = setCookies(site.cookies, cookies);
                await page.setViewport(site.size || defaultSize);
                await manageHeaders(site.headers);

                const tracker = new InflightRequests(page);
                await page.goto(doc[k].url, {waitUntil: 'networkidle2'}).catch(e => {
                    console.log('Navigation failed: ' + e.message);
                    const inflight = tracker.inflightRequests();
                    console.log(inflight.map(request => '  ' + request.url()).join('\n'));
                });
                tracker.dispose();

                if (site.waitFor) {
                    await page.waitFor(site.waitFor);
                }

                await page.screenshot({path: folder + k + '.png', fullPage: site.fullPage ? site.fullPage : true});
            }
        }
    } catch (e) {
        console.log(e);
    }

    await browser.close();
    console.log('Done!');

    return folder;
}

class InflightRequests {
    constructor(page) {
        this._page = page;
        this._requests = new Set();
        this._onStarted = this._onStarted.bind(this);
        this._onFinished = this._onFinished.bind(this);
        this._page.on('request', this._onStarted);
        this._page.on('requestfinished', this._onFinished);
        this._page.on('requestfailed', this._onFinished);
    }

    _onStarted(request) {
        this._requests.add(request);
    }

    _onFinished(request) {
        this._requests.delete(request);
    }

    inflightRequests() {
        return Array.from(this._requests);
    }

    dispose() {
        this._page.removeListener('request', this._onStarted);
        this._page.removeListener('requestfinished', this._onFinished);
        this._page.removeListener('requestfailed', this._onFinished);
    }
}

exports.shot = shot;
