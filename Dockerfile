# Base image
FROM debian:buster-slim

# Maintainer of this Dockerfile
MAINTAINER nicolas.poste@gmail.com

# see https://github.com/nodejs/docker-node
# and https://github.com/alekzonder/docker-puppeteer/blob/master/Dockerfile
RUN \
    # add user
    adduser --system --home /usr/screenshot/ screenshot && \
    apt-get update -y && apt-get install -y unzip curl && \
    # install node
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get -y install nodejs && \
    # install chrome and puppeteer
    apt-get install -yq gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 \
    libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 \
    libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 \
    libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 \
    ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget && \
    wget https://github.com/Yelp/dumb-init/releases/download/v1.2.1/dumb-init_1.2.1_amd64.deb && \
    dpkg -i dumb-init_*.deb && \
    # install jq
    apt-get install -y jq && \
    # install imagemagick
    apt-get -y install imagemagick && \
    # slim down image
    apt-get clean && apt-get autoremove -y && rm -f dumb-init_*.deb && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man/?? /usr/share/man/??_*

ENV HOME /usr/local/share
ENV PATH="$HOME/.yarn/bin:$PATH"

# yarn install
RUN curl -s -o- -L https://yarnpkg.com/install.sh | bash && \
    yarn global add puppeteer@1.8.0 && yarn global add js-yaml && yarn global add zip-folder && \
    # slim down image
    yarn cache clean

USER screenshot
WORKDIR /usr/screenshot

ENV PATH "/usr/local/share/.config/yarn/global/node_modules/.bin:$PATH"
ENV NODE_PATH "/usr/local/share/.config/yarn/global/node_modules"

COPY screen.js server.js local.js startscript.sh /usr/screenshot/

EXPOSE 8080

ENTRYPOINT ["dumb-init", "--"]

CMD ["node", "server.js"]
