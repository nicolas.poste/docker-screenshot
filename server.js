const http = require('http');
const path = require('path');
const fs = require('fs');
const zipFolder = require('zip-folder');
const rimraf = require('rimraf');

const { shot } = require('./screen.js');

const deletionCallback = (err) => {
    if (err) {
        throw err;
    }
    // if no error, file has been deleted successfully
};
function deleteFile(zipFileName) {
    fs.unlink(zipFileName, deletionCallback);
}

function deleteFolder(folder) {
    rimraf(folder, deletionCallback);
}

http.createServer(function (request, response) {
    if (request.method === 'POST') {

        const exec = require('child_process').exec;
        exec('startscript.sh');

        let body = '';
        request.on('data', function (data) {
            body += data;
        });
        request.on('end', function () {
            shot(body).then((folder) => {
                const zipFileName = `screenshots-${folder.replace(/\//g, '')}.zip`;
                zipFolder(folder, zipFileName, function (err) {
                    if (err) {
                        console.log(err);
                        response.writeHead(500, {'Content-Type': 'text/html'});
                        response.end('Error occurred when zipping...');
                    } else {
                        console.log('Success!');
                        response.writeHead(200, {'Content-Type': 'application/zip'});
                        fs.createReadStream(path.resolve('.', zipFileName))
                            .pipe(response);
                        deleteFile(zipFileName);
                        deleteFolder(folder);
                    }
                });
            });
        })
    } else if (request.method === 'DELETE') {
        console.info('Exiting!');
        response.writeHead(200, {'Content-Type': 'text/html'});
        response.end('Exiting!');
        process.exit(0);
    } else {
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write('Hello World!');
        response.end();
    }
}).listen(8080);

console.info('Application ready!');
