
const fs = require('fs');

const { shot } = require('./screen.js');

const args = process.argv.slice(2);
const yamlPath = args[0];
const outputPath = args[1];

try {
    const doc = fs.readFileSync(yamlPath, 'utf8');
    shot(doc).then((folder) => {
        fs.rename(folder, outputPath, function(err) {
            if (err) {
                console.log(err);
            } else {
                console.log('Success!');
            }
        });
    });
} catch (e) {
    console.log(e);
}
