
# Docker-screenshot

A simple docker image to extract screenshots from your website.  
A container can be used by your **CI tools**, such as *GitLab*.

## How to use

`docker run --rm -v ${pwd}:/screenshots -v ${pwd}/site.yml:/screenshot-config/site.yml registry.com/nicolas-poste/docker-screenshot`

You can provide a list of pages using a .yml file named `site.yml`.  
It must be located in `screenshot-config` folder on the docker container (you can either mount the file directly or the parent folder).

## Example of site.yml

### Simple example
```yml
google:
  url: "https://www.google.com"
  size:
    width: 1280
    height: 1024
```

### Complex example using timer and cookies

```yml
blog-talanlabs:
  url: "https://blog.talanlabs.com/"
  waitFor: 1000 # ms
  cookies:
    uselesscookie: # name of the cookie
      value: useless-value
      path: '/'
      domain: blog.talanlabs.com
      expires: 2147483647
  headers:
    Accept-Language: fr,en;q=0.9,en-US;q=0.8
    Accept-Content: application/json
```

## Local usage within GitLab

```yml
screen-preview:
  stage: screen
  image: <registry.com>/nicolas-poste/docker-screenshot
  script:
#   - launch-app
    - sleep 30 # wait for the website to be up and running
    - node local.js sites.yml screenshots/
```
