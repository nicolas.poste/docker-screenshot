#/bin/sh

# code for GitLab support within jobs
if [ -n "$CI_PROJECT_DIR" ] && [ -f "$CI_PROJECT_DIR"/hosts ] && [ ! -f /tmp/screenshot-initialized ]; then
  nc -l 9248
  cat $CI_PROJECT_DIR/hosts > /etc/hosts
  touch /tmp/screenshot-initialized
fi;
